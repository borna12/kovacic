// Memory Game
// © 2014 Nate Wiley
// License -- MIT
// best in full screen, works on phones/tablets (min height for game is 500px..) enjoy ;)
// Follow me on Codepen

(function() {
    //localStorage.clear();
    var br = 1;
    var sec = 0;
    var pokusaj = 0;
    var vrijeme = 1;

    var najbolje_vrijeme;
    var najmanji_broj_pokusaja;

    function pad(val) {
        return val > 9 ? val : "0" + val;
    }
    setInterval(function() {
        if (vrijeme == 1) {
            $("#seconds").html(pad(++sec % 60));
            $("#minutes").html(pad(parseInt(sec / 60, 10)));
        }
    }, 1000);
    var Memory = {

        init: function(cards) {
            this.$game = $(".game");
            this.$modal = $(".modal");
            this.$overlay = $(".modal-overlay");
            this.$restartButton = $(".restart");
            this.cardsArray = $.merge(cards, cards);

            this.shuffleCards(this.cardsArray);

            this.setup();
        },

        shuffleCards: function(cardsArray) {
            this.$cards = $(this.shuffle(this.cardsArray));
        },

        setup: function() {
            this.html = this.buildHTML();
            this.$game.html(this.html);
            this.$memoryCards = $(".card");
            this.binding();
            this.paused = false;
            this.guess = null;
            this.$cards = $(this.shuffle(this.cardsArray));

        },

        binding: function() {
            this.$memoryCards.on("click", this.cardClicked);
            this.$restartButton.on("click", $.proxy(this.reset, this));
        },
        // kinda messy but hey
        cardClicked: function() {

            var _ = Memory;
            var $card = $(this);
            if (!_.paused && !$card.find(".inside").hasClass("matched") && !$card.find(".inside").hasClass("picked")) {

                $card.find(".inside").addClass("picked");
                if (!_.guess) {
                    _.guess = $(this).attr("data-id");
                    $(this).find('p').toggle();
                } else if (_.guess == $(this).attr("data-id") && !$(this).hasClass("picked")) {
                    $(".picked").addClass("matched");
                    _.guess = null;
                    $(".matched").find('p').remove();
                    pokusaj++;


                    switch ($(this).attr('data-id')) {
                        case "1":
                            vrijeme = 0;
                            swal({
                                title: 'Mira Klobučar: Kampanule, 1955.',
                                html: '<img src="slike/mira Klobucar.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">Ovaj slikani prikaz ljubičastih zvončića, napravljen rukom i kistom Mire Klobučar smatra se prvom slikom koju je za svoju Zbirku djela hrvatskih slikarica rođenih u 19. stoljeću nabavio kolekcionar dr. sc. Josip Kovačić. S njom je oko 1965. započela povijest ove zbirke.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;

                            });
                            break;
                        case "2":
                            vrijeme = 0;
                            swal({
                                title: 'Jelka Struppi Wolkensperg: Autoportret u interijeru',
                                html: '<img src="slike/Jelka Struppi Wolkensperg.png" class="ikone"/>' +
                                    '<p style="text-align:justify;">Jelka Struppi bila je kćerka liječnika u Križevcima. Kao i Slava Raškaj rodila se gluha i nijema. Već je u školi za gluhonijeme u Grazu pokazala veliku nadarenost za crtanje. Sebe samu slika u ovom radu kao ozbiljnu damu, zatečenu pisanjem za svojim radnim stolom. Moguće je da je sliku izvela gledajući se u zrcalo.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;
                        case "3":
                            vrijeme = 0;
                            swal({
                                title: 'Nasta Rojc: Moj otac u uredu 1906. g.',
                                html: '<img src="slike/nastaRojc.png" class="ikone"/>' +
                                    '<p style="text-align:justify;">Otac Naste Rojc Milan bio je jako važna osoba pri osnivanju Akademije likovnih umjetnosti u Zagrebu 1907. U to vrijeme on je bio ministar obrazovanja koji je potpisao osnutak Akademije. Nema sumnje da je pri tome slušao i svoju kćerku Nastu, koja je već u Gimnaziji u Bjelovaru i uz podršku svog profesora risanja Josipa Hohnjeca postala sigurna da će se slikarstvom baviti do kraja života.</p>',
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;
                        case "4":
                            vrijeme = 0;
                            swal({
                                title: 'Anka Krizmanić: Majka kod rada, 1923.',
                                html: '<img src="slike/Anka krizmanic.png" class="ikone"/>' +
                                    '<p style="text-align:justify;">Slikarica Anka Krizmanić čitav je život crtala i bilježila izgled svojih najbližih – sestre Jelke i majke. Majku je portretirala od najranijih dana stvaranja sve do majčine smrti. Ovaj portret nastao je suprotstavljanjem tamnoga majčinog lika, pognutoga nad vezom, i okolnoga prostora doma donesenog plohama bojama.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;
                        case "5":
                            vrijeme = 0;
                            swal({
                                title: 'Vera Nikolić: Glava moje sestre Dagmar, oko 1907. g.',
                                html: '<img src="slike/vera nikolic.png" class="ikone"/>' +
                                    '<p style="text-align:justify;">Vera Nikolić bila je još učenica srednje škole kada je naslikala svoju mlađu sestru Dagmar. Josip Kovačić sakupio je puno Verinih radova, ali i fotografija obitelji Nikolić. Bilježio je i ono što su mu o umjetnici i njenoj sestri usmeno govorili nekadašnji članovi bogatoga kućanstva, primjerice kuharica Lizika Kovačec.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;
                        case "6":
                            vrijeme = 0;
                            swal({
                                title: 'Slava Raškaj: Žuti pijetao i bijela kokoš (Kokice), oko 1898.',
                                html: '<img src="slike/slava raskaj.png" class="ikone"/>' +
                                    '<p style="text-align:justify;">Ovaj je akvarel Josip Kovačić kupio od gospođe Vidoni iz Zagreba. Ona mu je ispričala da ga je od slikarice Slave Raškaj prije više od stotinu godina naručila gospođica Maixner. Ona je Slavi sama nabavila papir i time odredila veličinu rada te jasno rekla koji sadržaj (pijetao i kokica, jaja itd.) želi imati naslikan tehnikom vodenih boja na tom papiru.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;
                        case "7":
                            vrijeme = 0;
                            swal({
                                title: 'Vjera Bojničić: Seosko dvorište/Majur na Savskoj cesti, 1924. g.',
                                html: '<img src="slike/vjera bojnicic.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">Zagrebačku umjetnicu Vjeru Bojničić hvalili su zbog izrade ex-librisa (oznake vlasništva u knjizi) te slikanih obiteljskih i ostalih grbova. No, bila je i dobar kroničar izgleda Zagreba i njegove okolice. Ono što je nekada zaustavilo njenu pažnju – seosko imanje nadomak grada, danas su potpuno prekrile visoke zgrade, ceste, tramvajska pruga itd.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;
                        case "8":
                            vrijeme = 0;
                            swal({
                                title: 'Slava Raškaj: Ciča zima u šumi, oko 1899.',
                                html: '<img src="slike/slava raskaj.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">Slava Raškaj često je slikala u prirodi, čak i u zimskim mjesecima. Raditi na hladnoći tehnikom vodenih boja direktno na papir nije bilo nimalo lako. Poznato je da tijekom zime 1898./1899. napravila niz takvih radova i time čak ugrozila svoje zdravlje. Kolekcionar Josip Kovačić smatrao je da je tako ublažavala patnju zbog neuzvraćene ljubavi prema jednom mladiću, no to nećemo nikada sigurno znati.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;
                        case "9":
                            vrijeme = 0;
                            swal({
                                title: 'Slava Raškaj: Mrtva priroda s dimećom cigaretom,1896.',
                                html: '<img src="slike/slava raskaj2.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">Ovu, pomalo ukočenu kompoziciju, sastavljenu od ukrasnog posuđa, pepeljare s dimećom cigaretom i male ružice, stručno nazivamo mrtvom prirodom. Slava Raškaj naslikala ju je dok ju je slikanju uljanim bojama podučavao poznati hrvatski slikar Bela Čikoš Sessia. Ono što unosi napetost u ovaj prizor svakako je suprotstavljanje mrtvih stvari i još uvijek živog cvijeta u središtu rada.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;
                        case "10":
                            vrijeme = 0;
                            swal({
                                title: 'Lina Virant-Crnčić: Sveti Josip (za zastavu sisačkih obrtnika), 1936. g.',
                                html: '<img src="slike/Lina Virant.crncic.png" class="ikone"/>' +
                                    '<p style="text-align:justify;">Lina Virant Crnčić osmislila je za vezenje na zastavi sisačkih obrtnika lik sv. Josipa. Poznato je kako se ovog sveca smatra zaštitnikom radnika, tesara, blagajnika, inženjera i graditelja. Stoga ne čudi odabir njega, Lina Virant prikazala ga je kao starijeg muškarca koji se u svojoj radionici bavi stolarskim radom.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;


                    }

                } else {
                    pokusaj++;
                    $(this).find('p').toggle();
                    _.guess = null;
                    _.paused = true;
                    setTimeout(function() {
                        $(".picked").removeClass("picked");
                        Memory.paused = false;
                        $(".brojevi").show();
                    }, 1200);
                }
                if ($(".matched").length == $(".card").length) {
                    _.win();
                }
            }
        },

        win: function() {
            this.paused = true;
            setTimeout(function() {
                Memory.showModal();
                Memory.$game.fadeOut();

            }, 1000);
        },

        showModal: function() {
            var minute = Math.floor(sec / 60);
            var sekunde = sec - minute * 60;
            this.$overlay.show();
            this.$modal.fadeIn("slow");


            var najvrijeme = localStorage.getItem('najvrijeme');

            if (najvrijeme === undefined || najvrijeme === null) {
                najvrijeme = sec;
                localStorage.setItem('najvrijeme', sec);
            }

            // If the user has more points than the currently stored high score then
            if (sec < najvrijeme) {
                // Set the high score to the users' current points
                najvrijeme = sec;
                // Store the high score
                localStorage.setItem('najvrijeme', sec);
            }

            

            // Return the high score

            var najpokusaji = localStorage.getItem('najpokusaji');

            if (najpokusaji === undefined || najpokusaji === null) {
                najpokusaji = pokusaj;
                localStorage.setItem('najpokusaji', pokusaj);
            }

            // If the user has more points than the currently stored high score then
            if (pokusaj < najpokusaji) {
                // Set the high score to the users' current points
                najpokusaji = pokusaj;
                // Store the high score
                localStorage.setItem('najpokusaji', pokusaj);
            }
            var naj_minute = Math.floor(najvrijeme / 60);
            var naj_sekunde = najvrijeme - naj_minute * 60;

            $(".time").html("<br>trenutačni broj pokušaja: " + pokusaj + "</br>najmanji broj pokušaja u igri: " + najpokusaji + "</br></br>vrijeme potrebno za završetak igre: " + minute + " min " + sekunde + " sec</br>najbolje vrijeme: " + naj_minute + " min " + naj_sekunde + " sec</br></br></br><p><a href='../index.html' style='color:black;'>odaberite drugi tip igre</a></p>");
        },

        hideModal: function() {
            this.$overlay.hide();
            this.$modal.hide();
        },

        reset: function() {
            this.hideModal();
            this.shuffleCards(this.cardsArray);
            this.setup();
            this.$game.show("slow");
            pokusaj = 0;
            sec = 0;
            br = 1;
        },

        // Fisher--Yates Algorithm -- http://bost.ocks.org/mike/shuffle/
        shuffle: function(array) {
            var counter = array.length,
                temp, index;
            // While there are elements in the array
            while (counter > 0) {
                // Pick a random index
                index = Math.floor(Math.random() * counter);
                // Decrease counter by 1
                counter--;
                // And swap the last element with it
                temp = array[counter];
                array[counter] = array[index];
                array[index] = temp;
            }
            return array;
        },

        buildHTML: function() {

            var frag = '';
            br = 1;
            var lista_slika = [];
            var lista_imena = [];
            this.$cards.each(function(k, v) {
                if (Math.floor((Math.random() * 2) + 1) == 1) {
                    if ($.inArray(v.name, lista_imena) == -1) {

                        frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><span>' + v.name + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
                        if (br < cards.length) {
                            br++;
                        };

                        lista_imena.push(v.name);


                    } else {
                        frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name + '" /></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
                        if (br < cards.length) {
                            br++;
                        };

                        lista_slika.push(v.img);

                    }
                } else {
                    if ($.inArray(v.img, lista_slika) == -1) {

                        frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name + '" /></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
                        if (br < cards.length) {
                            br++;
                        };

                        lista_slika.push(v.img);


                    } else {
                        frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><span>' + v.name + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
                        if (br < cards.length) {
                            br++;
                        };

                        lista_imena.push(v.name);

                    }
                }
            });
            return frag;
        }
    };

    var cards = [{
        name: "Cvijeće",
        img: "slike/mira Klobucar.jpg",
        id: 1,
    }, {
        name: "Autoportret",
        img: "slike/Jelka Struppi Wolkensperg.png",
        id: 2
    }, {
        name: "Portret oca",
        img: "slike/nastaRojc.png",
        id: 3
    }, {
        name: "Portret majke",
        img: "slike/Anka krizmanic.png",
        id: 4
    }, {
        name: "Dječji portret",
        img: "slike/vera nikolic.png",
        id: 5
    }, {
        name: "Životinje",
        img: "slike/slava raskaj.png",
        id: 6
    }, {
        name: "Pejzaž s arhitekturom",
        img: "slike/vjera bojnicic.jpg",
        id: 7
    }, {
        name: "Zimski pejzaž",
        img: "slike/slava raskaj.jpg",
        id: 8
    }, {
        name: "Mrtva priroda",
        img: "slike/slava raskaj2.jpg",
        id: 9
    }, {
        name: "Kršćanski motiv - Sv. Josip",
        img: "slike/Lina Virant.crncic.png",
        id: 10
    }];

    var brojKarata = cards.length;
    Memory.init(cards);


})();