// Memory Game
// © 2014 Nate Wiley
// License -- MIT
// best in full screen, works on phones/tablets (min height for game is 500px..) enjoy ;)
// Follow me on Codepen

(function() {
    //localStorage.clear();
    var br = 1;
    var sec = 0;
    var pokusaj = 0;
    var vrijeme = 1;

    var najbolje_vrijeme;
    var najmanji_broj_pokusaja;

    function pad(val) {
        return val > 9 ? val : "0" + val;
    }
    setInterval(function() {
        if (vrijeme == 1) {
            $("#seconds").html(pad(++sec % 60));
            $("#minutes").html(pad(parseInt(sec / 60, 10)));
        }
    }, 1000);
    var Memory = {

        init: function(cards) {
            this.$game = $(".game");
            this.$modal = $(".modal");
            this.$overlay = $(".modal-overlay");
            this.$restartButton = $(".restart");
            this.cardsArray = $.merge(cards, cards);

            this.shuffleCards(this.cardsArray);

            this.setup();
        },

        shuffleCards: function(cardsArray) {
            this.$cards = $(this.shuffle(this.cardsArray));
        },

        setup: function() {
            this.html = this.buildHTML();
            this.$game.html(this.html);
            this.$memoryCards = $(".card");
            this.binding();
            this.paused = false;
            this.guess = null;
            this.$cards = $(this.shuffle(this.cardsArray));

        },

        binding: function() {
            this.$memoryCards.on("click", this.cardClicked);
            this.$restartButton.on("click", $.proxy(this.reset, this));
        },
        // kinda messy but hey
        cardClicked: function() {

            var _ = Memory;
            var $card = $(this);
            if (!_.paused && !$card.find(".inside").hasClass("matched") && !$card.find(".inside").hasClass("picked")) {

                $card.find(".inside").addClass("picked");
                if (!_.guess) {
                    _.guess = $(this).attr("data-id");
                    $(this).find('p').toggle();
                } else if (_.guess == $(this).attr("data-id") && !$(this).hasClass("picked")) {
                    $(".picked").addClass("matched");
                    _.guess = null;
                    $(".matched").find('p').remove();
                    pokusaj++;


                    switch ($(this).attr('data-id')) {
                        case "1":
                            vrijeme = 0;
                            swal({
                                title: 'Fany Daubachy: Grm tikve na kolcu',
                                html: '<img src="slike/fancy Daubachy.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">Fany Daubachy imala je troje djece i bolesnog supruga i nije imala previše vremena za slikanje. Stoga je vjerojatno kako bi olovkom brže zabilježila motiv uočen u prirodi, a kasnije ga u svom domu dotjerivala do najmanjeg detalja. Koristila je i tehniku raspršivanja grafita olovke četkicom i od tuda mekoća i dubina njenih cvjetova bundeve.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;

                            });
                            break;
                        case "2":
                            vrijeme = 0;
                            swal({
                                title: 'Jelka Struppi Wolkensperg: Žena s krčagom',
                                html: '<img src="slike/jelka struppi Wolkensperg 3.png" class="ikone"/>' +
                                    '<p style="text-align:justify;">Umjetnica je studirala na Likovnoj akademiji u Münchenu i vjerojatno je tada napravila ovu studiju kostimirane žene u tehnici tuša. Riječ je o mokroj crtaćoj tehnici, jer se umjetnik na površini papira izražava umakanjem pera, zašiljene trske ili četkice u tekućinu. Jelka tušem ostvaruje slikarski učinak – nijansiranjem crnila postiže dubinu, a tankim linijama volumene oblika.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;
                        case "3":
                            vrijeme = 0;
                            swal({
                                title: 'Zdenka Pexidr-Srića: Portret djevojke u profilu, 1911.',
                                html: '<img src="slike/zdenka pexidr.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">Umjetnica je napravila ovaj rad tijekom studija na Likovnoj akademiji u Münchenu. Vješto je iskoristila prirodnu mekoću ugljenog štapića u sjenkama na obrazu i nosu, djevojčinoj vrpci oko vrata itd. Tamnoj kosi dala je posebnu snagu jer ju je izrazila s najvećom količinom ugljena. Crtež je na kraju morao biti učvršćen kemikalijom da se zrnca ugljena ne rasprše.</p>',
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;
                        case "4":
                            vrijeme = 0;
                            swal({
                                title: 'Anka Krizmanić: Izlet na Plitvice, 1922./79.',
                                html: '<img src="slike/Anka krizmanic 5.png" class="ikone"/>' +
                                    '<p style="text-align:justify;">Godine 1922. Anka Krizmanić boravila je na Plitvicama i tamo izradila niz djela koristeći tehniku pastela ili štapića suhe boje. Većinom je bilježila ljepote prirode i jezera. No, u ovom radu vrlo je izražajno pastelom predstavila zbijenu grupu izletnika koji se voze na kolima. Pažnju privlači njeno majstorsko korištenje bijelih i svijetložutih štapića kako bi se istakla lagana ljetna odjeća. Rad je također morao biti učvršćen kemikalijom.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;
                        case "5":
                            vrijeme = 0;
                            swal({
                                title: 'Mira Mayr-Marochino: Slikarski pribor',
                                html: '<img src="slike/mira mayr2.jpg.png" class="ikone"/>' +
                                    '<p style="text-align:justify;">Slikanje uljem na platnu zahtijeva posebno likovno obrazovanje, a često i istraživanje različitih vrsta boje, ulja i razrjeđivača. Slikarica Mira Mayr – Marochino prikazala je osnovni materijal i pribor za slikanje uljanim bojama - boje u tubama, terpentin i kistove, ali i pokazala kakvu punoću i dubinu one mogu ostvariti. U oblini vrča s kistovima nazire se i unutrašnjost njenog atelijera.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;
                        case "6":
                            vrijeme = 0;
                            swal({
                                title: 'Slava Raškaj: Kupa kod Ozlja, oko 1899.',
                                html: '<img src="slike/kupa.png" class="ikone"/>' +
                                    '<p style="text-align:justify;">Slava Raškaj svoje je najbolje radove naslikala u tehnici akvarela. Ona zahtijeva dobro oko za iskorištavanje bjeline papira i razrjeđivanje boja te vještu ruku koja će ih moći nanositi u tankim slojevima. Njena Kupa, slikana s visokog mjesta, samo je na prvi pogled potpuno zasićena zeleno-plavim tonovima. U njoj žive mali bijeli detalji papira (raslinje uz rijeku, trag brzaca i sl.), ali i čitava prozirna vrpca neba.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;
                        case "7":
                            vrijeme = 0;
                            swal({
                                title: 'Anka Krizmanić: Most na Elbi u Dresdenu, 1914.',
                                html: '<img src="slike/drvorez.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">Anka Krizmanić savladala je tehniku drvoreza već kod umjetnika Tomislava Krizmana u Zagrebu,a  koji je i sam bio odličan grafičar. Drvorez znači izdubljivanje drveta nožićem i ostavljanje netaknutim onih dijelova koji će nositi određenu boju pri otiskivanju. Ovaj rad nastao je tijekom njenog školovanja na Obrtnoj školi u Dresdenu. Plavetnilo rijeke Elbe zapravo je u prvom platnu, dok je sam most potisnut, gotovo kao ornament, u pozadinu.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;
                        case "8":
                            vrijeme = 0;
                            swal({
                                title: 'Tereza Paulić: Plesačice među palmama, oko 1926.',
                                html: '<img src="slike/skarorez.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">Umjetničko rezanje papira škarama ili škarorez vrlo je stara tehnika. Upotrijebljena je prvo u Kini nakon otkrića papira u 4. st. Kasnije se u Europi koristila za izradu crkvenih motiva ili uzoraka za vez. U početku je bila dio odgoja bogatih žena, kasnije i svih ostalih. Tereza Paulić, stručna učiteljica, kasnije restauratorica i kustosica Etnografskog muzeja u Zagrebu, s uspjehom ju je koristila za svoje radove oko 1926.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;
                        case "9":
                            vrijeme = 0;
                            swal({
                                title: 'Slava Raškaj: Autoportretna frizura potiljka Slave Raškaj u dijademi od cvjetova jasmina i visibaba, 1883.',
                                html: '<img src="slike/pamuk.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">Vezenje različitim materijalima na tkanini stara je ženska aktivnost raširena po svijetu, pa tako i u Hrvatskoj. Rezultati vezenja – vezovi – mogu imati snagu umjetničkih radova, bilo da se radi o geometrijskim motivima, bilo pravim slikama izrađenima tehnikom veza. Slava Raškaj, kao i sve djevojčice krajem 19. st, morala je savladati vezenje. No, ona je u ranoj dobi mogla napraviti i više – načiniti izgled svoje glave s leđa, koristeći niti vlastite smeđe kose.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;
                        case "10":
                            vrijeme = 0;
                            swal({
                                title: 'Nasta Rojc: Predah, 1910',
                                html: '<img src="slike/bronca.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">Izrada skulpture u bronci podrazumijeva prolaženje kroz nekoliko postupaka – skiciranje ideje u crtežu, oblikovanje željenog oblika u glini, moguće i gipsu te donošenje odluke o lijevanju u bronci. Ovaj uspjeli prikaz starijeg čovjeka pri odmoru pripada bečkom razdoblju školovanja Naste Rojc (1908. – 1911.) i radu s profesorom kiparskog modeliranja Kaufungenom.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;


                    }

                } else {
                    pokusaj++;
                    $(this).find('p').toggle();
                    _.guess = null;
                    _.paused = true;
                    setTimeout(function() {
                        $(".picked").removeClass("picked");
                        Memory.paused = false;
                        $(".brojevi").show();
                    }, 1200);
                }
                if ($(".matched").length == $(".card").length) {
                    _.win();
                }
            }
        },

        win: function() {
            this.paused = true;
            setTimeout(function() {
                Memory.showModal();
                Memory.$game.fadeOut();

            }, 1000);
        },

        showModal: function() {
            var minute = Math.floor(sec / 60);
            var sekunde = sec - minute * 60;
            this.$overlay.show();
            this.$modal.fadeIn("slow");


            var najvrijeme = localStorage.getItem('najvrijeme');

            if (najvrijeme === undefined || najvrijeme === null) {
                najvrijeme = sec;
                localStorage.setItem('najvrijeme', sec);
            }

            // If the user has more points than the currently stored high score then
            if (sec < najvrijeme) {
                // Set the high score to the users' current points
                najvrijeme = sec;
                // Store the high score
                localStorage.setItem('najvrijeme', sec);
            }

            

            // Return the high score

            var najpokusaji = localStorage.getItem('najpokusaji');

            if (najpokusaji === undefined || najpokusaji === null) {
                najpokusaji = pokusaj;
                localStorage.setItem('najpokusaji', pokusaj);
            }

            // If the user has more points than the currently stored high score then
            if (pokusaj < najpokusaji) {
                // Set the high score to the users' current points
                najpokusaji = pokusaj;
                // Store the high score
                localStorage.setItem('najpokusaji', pokusaj);
            }
            var naj_minute = Math.floor(najvrijeme / 60);
            var naj_sekunde = najvrijeme - naj_minute * 60;

            $(".time").html("<br>trenutačni broj pokušaja: " + pokusaj + "</br>najmanji broj pokušaja u igri: " + najpokusaji + "</br></br>vrijeme potrebno za završetak igre: " + minute + " min " + sekunde + " sec</br>najbolje vrijeme: " + naj_minute + " min " + naj_sekunde + " sec</br></br></br><p><a href='../index.html' style='color:black;'>odaberite drugi tip igre</a></p>");
        },

        hideModal: function() {
            this.$overlay.hide();
            this.$modal.hide();
        },

        reset: function() {
            this.hideModal();
            this.shuffleCards(this.cardsArray);
            this.setup();
            this.$game.show("slow");
            pokusaj = 0;
            sec = 0;
            br = 1;
        },

        // Fisher--Yates Algorithm -- http://bost.ocks.org/mike/shuffle/
        shuffle: function(array) {
            var counter = array.length,
                temp, index;
            // While there are elements in the array
            while (counter > 0) {
                // Pick a random index
                index = Math.floor(Math.random() * counter);
                // Decrease counter by 1
                counter--;
                // And swap the last element with it
                temp = array[counter];
                array[counter] = array[index];
                array[index] = temp;
            }
            return array;
        },

        buildHTML: function() {

            var frag = '';
            br = 1;
            var lista_slika = [];
            var lista_imena = [];
            this.$cards.each(function(k, v) {
                if (Math.floor((Math.random() * 2) + 1) == 1) {
                    if ($.inArray(v.name, lista_imena) == -1) {

                        frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><span>' + v.name + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
                        if (br < cards.length) {
                            br++;
                        };

                        lista_imena.push(v.name);


                    } else {
                        frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name + '" /></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
                        if (br < cards.length) {
                            br++;
                        };

                        lista_slika.push(v.img);

                    }
                } else {
                    if ($.inArray(v.img, lista_slika) == -1) {

                        frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name + '" /></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
                        if (br < cards.length) {
                            br++;
                        };

                        lista_slika.push(v.img);


                    } else {
                        frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><span>' + v.name + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
                        if (br < cards.length) {
                            br++;
                        };

                        lista_imena.push(v.name);

                    }
                }
            });
            return frag;
        }
    };

    var cards = [{
        name: "Olovka na papiru",
        img: "slike/fancy Daubachy.jpg",
        id: 1,
    }, {
        name: "Tuš",
        img: "slike/jelka struppi Wolkensperg 3.png",
        id: 2
    }, {
        name: "Ugljen",
        img: "slike/zdenka pexidr.jpg",
        id: 3
    }, {
        name: "Pastel",
        img: "slike/Anka krizmanic 5.png",
        id: 4
    }, {
        name: "Ulje na platnu",
        img: "slike/mira mayr2.jpg.png",
        id: 5
    }, {
        name: "Akvarel",
        img: "slike/kupa.png",
        id: 6
    }, {
        name: "Drvorez",
        img: "slike/drvorez.jpg",
        id: 7
    }, {
        name: "Škarorez",
        img: "slike/skarorez.jpg",
        id: 8
    }, {
        name: "Vez pamukom i kosom",
        img: "slike/pamuk.jpg",
        id: 9
    }, {
        name: "Lijevanje u bronci",
        img: "slike/bronca.jpg",
        id: 10
    }];

    var brojKarata = cards.length;
    Memory.init(cards);


})();