// Memory Game
// © 2014 Nate Wiley
// License -- MIT
// best in full screen, works on phones/tablets (min height for game is 500px..) enjoy ;)
// Follow me on Codepen

(function() {
    //localStorage.clear();
    var br = 1;
    var sec = 0;
    var pokusaj = 0;
    var vrijeme = 1;

    var najbolje_vrijeme;
    var najmanji_broj_pokusaja;

    function pad(val) {
        return val > 9 ? val : "0" + val;
    }
    setInterval(function() {
        if (vrijeme == 1) {
            $("#seconds").html(pad(++sec % 60));
            $("#minutes").html(pad(parseInt(sec / 60, 10)));
        }
    }, 1000);
    var Memory = {

        init: function(cards) {
            this.$game = $(".game");
            this.$modal = $(".modal");
            this.$overlay = $(".modal-overlay");
            this.$restartButton = $(".restart");
            this.cardsArray = $.merge(cards, cards);

            this.shuffleCards(this.cardsArray);

            this.setup();
        },

        shuffleCards: function(cardsArray) {
            this.$cards = $(this.shuffle(this.cardsArray));
        },

        setup: function() {
            this.html = this.buildHTML();
            this.$game.html(this.html);
            this.$memoryCards = $(".card");
            this.binding();
            this.paused = false;
            this.guess = null;
            this.$cards = $(this.shuffle(this.cardsArray));

        },

        binding: function() {
            this.$memoryCards.on("click", this.cardClicked);
            this.$restartButton.on("click", $.proxy(this.reset, this));
        },
        // kinda messy but hey
        cardClicked: function() {

            var _ = Memory;
            var $card = $(this);
            if (!_.paused && !$card.find(".inside").hasClass("matched") && !$card.find(".inside").hasClass("picked")) {

                $card.find(".inside").addClass("picked");
                if (!_.guess) {
                    _.guess = $(this).attr("data-id");
                    $(this).find('p').toggle();
                } else if (_.guess == $(this).attr("data-id") && !$(this).hasClass("picked")) {
                    $(".picked").addClass("matched");
                    _.guess = null;
                    $(".matched").find('p').remove();
                    pokusaj++;


                    switch ($(this).attr('data-id')) {
                        case "1":
                            vrijeme = 0;
                            swal({
                                title: 'Nevenka  Đorđević-Tomašević: Studija za autoportret, oko 1938.',
                                html: '<img src="slike/nevenka.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">Nevenka Đorđević došla je u Zagreb kao mlada djevojka 1915. godine. Prvo je prošla privatnu obuku kod slikara Ljube Babića, a 1916. upisuje se na Akademiju likovnih umjetnosti koju uspješno završava. U trenutku kada razmahanim potezom kista i crnom bojom na narančastoj plohi nastaje ovaj brzi autoportret, ona je poznata slikarica. No, ona je i dobra i uspješna keramičarka – umjetnica koja oblikuje predmete od gline.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;

                            });
                            break;
                        case "2":
                            vrijeme = 0;
                            swal({
                                title: 'Dora Car: Autoportret, 1912.',
                                html: '<img src="slike/dora.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">Zagrepčanka Dora Car odlučila se, kao i njen brat Bogumil, školovati na Likovnoj akademiji u rodnom gradu. Poslije diplomiranja, 1913. radila je do penzije kao učiteljica crtanja u Ženskom liceju i Drugoj ženskoj gimnaziji. U trenutku kad nastaje ovaj portret ona je još studentica. U svojim sjećanjima brat Bogumil zapisao je da je to bilo njeno najbolje slikarsko razdoblje, puno zanosa i sigurnosti u sebe samu. A tako i izgleda na ovoj slici.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;
                        case "3":
                            vrijeme = 0;
                            swal({
                                title: 'Zdenka Pexidr-Srića: Autoportret, 1912.',
                                html: '<img src="slike/zdenka.png" class="ikone"/>' +
                                    '<p style="text-align:justify;">Zdenka Pexidr-Srića pripada prvoj generaciji studenata Akademije likovnih umjetnosti u Zagrebu, upisanoj 1907 godine. Kasnije dopunjava obrazovanje i na Akademiji u Münchenu. Velika je vjerojatnost da je ovaj svoj autoportret radila tamo. Njime pokazuje zavidnu vještinu opažanja i crtanja ljudskog lica i to koristeći ugljeni štapić. Njene oči u ovom radu zadržavaju živošću, a najdublje je crnilo upotrijebila za baršunastu vrpcu za kosu.</p>',
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;
                        case "4":
                            vrijeme = 0;
                            swal({
                                title: 'Mira Mayr-Marochino: Portret djevojke / Autoportret iz studentskih dana',
                                html: '<img src="slike/mira.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">O umjetnici nemamo puno podataka. Bavila se kopiranjem tuđih djela i restauriranjem oštećenih radova. Ovo zadnje zahtijevalo je veliko poznavanje tehnologije slikanja, koju je savladala u privatnim slikarskim školama i na Likovnoj akademiji u Zagrebu. Prema mišljenju kolekcionara Josipa Kovačića ovo je njen autoportret. Radila ga je mlada osoba koja je savladala tonsko slikanje (slikanje bojom različite svjetline) i bila dobar portretist.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;
                        case "5":
                            vrijeme = 0;
                            swal({
                                title: 'Anka Krizmanić: Ljubavnici, 1926.',
                                html: '<img src="slike/anka.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">Kolekcionara Josipa Kovačića i Anku Krizmanić spajalo je dugogodišnje poznanstvo. U starosti, kad su je mnogi izbjegavali, on je satima slušao njena sjećanja. Ona su potvrdila da je na ovom pastelu, prepunom energije koja struji između dvoje zaljubljenih ljudi, portretirala sebe s leđa. Zato smo ovaj pastel smjestili među autoportrete. Dakako, važnijim od izgleda portretiranih, ovdje je bilo likovno opisivanje njihovih osjećaja.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;
                        case "6":
                            vrijeme = 0;
                            swal({
                                title: 'Nasta Rojc: Autoportret u bijeloj košulji, oko 1925.',
                                html: '<img src="slike/nasta.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">Nasta Rojc rođena je u Bjelovaru, gdje je u Gimnaziji dobila likovnu poduku. Strogi otac omogućio joj je školovanje na Ženskoj umjetničkoj školi u Beču i Münchenu. Bila je odlična portretistica, ali je često slikala i interpretirala i sebe samu. Na ovom autoportretu, načinjenom u profilu te uspostavljenom na kontrastu snježno bijele košulje i tamne pozadine koja je okružuje, ona izgleda kao u sebe sigurna, ali ne i radosna sredovječna žena.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;
                        case "7":
                            vrijeme = 0;
                            swal({
                                title: 'Rita Bottura Bersa: Autoportret, oko 1895.',
                                html: '<img src="slike/rita.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">Malo je biografskih podataka o Riti Bottura, kćerki zadarskog profesora. Rođena je u Zadru, gdje je sa sestrom Ginom polazila privatni slikarski tečaj. Kasnije se udala za kipara Bruna Bersu. U njemu je, nema sumnje, imala podršku za daljnji umjetnički rad. Autoportret iz 1895., a koji je izlagan na važnoj I. dalmatinskoj izložbi 1908. u Splitu, reprezentativan je djevojački portret, nastao u otvorenome svijetlom prostoru u tehnici pastela.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;
                        case "8":
                            vrijeme = 0;
                            swal({
                                title: 'Elizabeta Drašković (1881. – 1962.)',
                                html: '<img src="slike/elizabeta.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">Elizabeta Drašković pravi je primjer hrvatske slikarice rođene u 19. stoljeću. Bila je plemkinja i likovno obrazovanje bilo joj je dio odgoja. Polazila je slikarski tečaj Tomislava Krizmana u Zagrebu, na Jelačićevom trgu br. 3. Do tamo bi je redovito pratio i štitio Jean, osiromašeni rođak. U autoportretu nas ne gleda u oči nego je udubljena u nešto, najvjerojatnije izradu crteža. Koristi pastele i crnu kredu, čije učinke najviše opažamo u lijepom šeširu.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;
                        case "9":
                            vrijeme = 0;
                            swal({
                                title: 'Vera Nikolić Podrinska: Autoportret, 1955.',
                                html: '<img src="slike/vera.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">Vera Nikolić Podrinska odrastala je u bogatoj zagrebačkoj obitelji. Slikanje je učila privatno, a poslije na Likovnoj akademiji u Zagrebu. Godine 1933. preuzela je brigu za vođenje velikoga gospodarstva, posebno vinograda, na Prekrižju. Puno je putovala. Svoje bogato životno iskustvo, pa i dokazanu brigu za druge, daje nam naslutiti na ovom, potpuno koloristički donesenom autoportretu.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;
                        case "10":
                            vrijeme = 0;
                            swal({
                                title: 'Iva Simonović: Autoportret za kolegicu Zdenku Pexidr, oko 1916.',
                                html: '<img src="slike/iva.jpg" class="ikone"/>' +
                                    '<p style="text-align:justify;">Iva Simonović pohađala je Ženski tečaj za crtanje i obrt na Obrtnoj školi u Zagrebu. Godine 1907. upisala je Likovnu akademiju. Ubrzo ju je napustila, no likovno stvaranje nije. Opredijelila se za oblikovanje medalja i plaketa. Slično plaketi, izrađuje u gipsu i ovaj autoportret. Riječ je o nježnom djevojačkom liku u profilu, kojeg namjenjuje kolegici Zdenki Pexidr-Srića kao uspomenu na zajedničku izložbu u Zagrebu 1916.</p>',
                                showCloseButton: true,
                                confirmButtonText: 'dalje',
                            }, function(isConfirm) {

                            });
                            $('.swal2-confirm').click(function() {
                                vrijeme = 1;
                            });
                            break;


                    }

                } else {
                    pokusaj++;
                    $(this).find('p').toggle();
                    _.guess = null;
                    _.paused = true;
                    setTimeout(function() {
                        $(".picked").removeClass("picked");
                        Memory.paused = false;
                        $(".brojevi").show();
                    }, 1200);
                }
                if ($(".matched").length == $(".card").length) {
                    _.win();
                }
            }
        },

        win: function() {
            this.paused = true;
            setTimeout(function() {
                Memory.showModal();
                Memory.$game.fadeOut();

            }, 1000);
        },

        showModal: function() {
            var minute = Math.floor(sec / 60);
            var sekunde = sec - minute * 60;
            this.$overlay.show();
            this.$modal.fadeIn("slow");


            var najvrijeme = localStorage.getItem('najvrijeme');

            if (najvrijeme === undefined || najvrijeme === null) {
                najvrijeme = sec;
                localStorage.setItem('najvrijeme', sec);
            }

            // If the user has more points than the currently stored high score then
            if (sec < najvrijeme) {
                // Set the high score to the users' current points
                najvrijeme = sec;
                // Store the high score
                localStorage.setItem('najvrijeme', sec);
            }

            

            // Return the high score

            var najpokusaji = localStorage.getItem('najpokusaji');

            if (najpokusaji === undefined || najpokusaji === null) {
                najpokusaji = pokusaj;
                localStorage.setItem('najpokusaji', pokusaj);
            }

            // If the user has more points than the currently stored high score then
            if (pokusaj < najpokusaji) {
                // Set the high score to the users' current points
                najpokusaji = pokusaj;
                // Store the high score
                localStorage.setItem('najpokusaji', pokusaj);
            }
            var naj_minute = Math.floor(najvrijeme / 60);
            var naj_sekunde = najvrijeme - naj_minute * 60;

            $(".time").html("<br>trenutačni broj pokušaja: " + pokusaj + "</br>najmanji broj pokušaja u igri: " + najpokusaji + "</br></br>vrijeme potrebno za završetak igre: " + minute + " min " + sekunde + " sec</br>najbolje vrijeme: " + naj_minute + " min " + naj_sekunde + " sec</br></br></br><p><a href='../index.html' style='color:black;'>odaberite drugi tip igre</a></p>");
        },

        hideModal: function() {
            this.$overlay.hide();
            this.$modal.hide();
        },

        reset: function() {
            this.hideModal();
            this.shuffleCards(this.cardsArray);
            this.setup();
            this.$game.show("slow");
            pokusaj = 0;
            sec = 0;
            br = 1;
        },

        // Fisher--Yates Algorithm -- http://bost.ocks.org/mike/shuffle/
        shuffle: function(array) {
            var counter = array.length,
                temp, index;
            // While there are elements in the array
            while (counter > 0) {
                // Pick a random index
                index = Math.floor(Math.random() * counter);
                // Decrease counter by 1
                counter--;
                // And swap the last element with it
                temp = array[counter];
                array[counter] = array[index];
                array[index] = temp;
            }
            return array;
        },

        buildHTML: function() {

            var frag = '';
            br = 1;
            var lista_slika = [];
            var lista_imena = [];
            this.$cards.each(function(k, v) {
                if (Math.floor((Math.random() * 2) + 1) == 1) {
                    if ($.inArray(v.name, lista_imena) == -1) {

                        frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><span>' + v.name + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
                        if (br < cards.length) {
                            br++;
                        };

                        lista_imena.push(v.name);


                    } else {
                        frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name + '" /></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
                        if (br < cards.length) {
                            br++;
                        };

                        lista_slika.push(v.img);

                    }
                } else {
                    if ($.inArray(v.img, lista_slika) == -1) {

                        frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name + '" /></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
                        if (br < cards.length) {
                            br++;
                        };

                        lista_slika.push(v.img);


                    } else {
                        frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><span>' + v.name + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
                        if (br < cards.length) {
                            br++;
                        };

                        lista_imena.push(v.name);

                    }
                }
            });
            return frag;
        }
    };

    var cards = [{
        name: "Nevenka Đorđević-Tomašević",
        img: "slike/nevenka.jpg",
        id: 1,
    }, {
        name: "Dora Car",
        img: "slike/dora.jpg",
        id: 2
    }, {
        name: "Zdenka Pexidr-Srića",
        img: "slike/zdenka.png",
        id: 3
    }, {
        name: "Mira Mayr-Marrochino",
        img: "slike/mira.jpg",
        id: 4
    }, {
        name: "Anka Krizmanić",
        img: "slike/anka.jpg",
        id: 5
    }, {
        name: "Nasta Rojc",
        img: "slike/nasta.jpg",
        id: 6
    }, {
        name: "Rita Bersa Bottura",
        img: "slike/rita.jpg",
        id: 7
    }, {
        name: "Elizabeta Drašković",
        img: "slike/elizabeta.jpg",
        id: 8
    }, {
        name: "Vera Nikolić Podrinska",
        img: "slike/vera.jpg",
        id: 9
    }, {
        name: "Iva Simonović",
        img: "slike/iva.jpg",
        id: 10
    }];

    var brojKarata = cards.length;
    Memory.init(cards);


})();